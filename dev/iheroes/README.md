# iHeroes

Esse teste foi realizado para executar ações solicitadas para um modelo de sistema de gerenciamento de distribuição de Heros para combater ameaças.

  *Segue abaixo instruções para execução e verificação de todas as etapas realizadas.*

## Pre-requisitos

Para rodar a aplicação se fará necessário que em sua máquina você tenha:

*Ruby 2.6.3* ou mais recente
*Rails 6.0.0* ou mais recente
*PostgreSQL 10.7* ou mais recente
Caso não possua essas versões você pode visitar esta página: https://gorails.com/setup/ubuntu/18.10 com poucos passos para seguir será possivel realizar a instalação para o Sistema Operacional que você utilizar.

## Instalação

Verifique que você tem todas as versões necessárias em sua máquina de Ruby, Ruby on Rails e PostgreSQL.

Clone este repositorio em sua máquina no terminal:

```
$ git clone git@github.com:hmdros/iheroes.git
```
Abra a pasta que você acabou de clonar:
```
$ cd iheroes/
```
Certifique-se de que está usando a versão indicada do Ruby (2.6.3) dentro desta pasta: 
```
ruby -v
```

Instale todas as dependências de gemas do Ruby:
```
$ bundle install
```
Criando o Banco de Dados:
```
$ rails db:create
```
Rodando todas as migrações necessárias:
```
$ rails db:migrate
```
Alimentando o banco de dados com algumas informações básicas e criando um usuário inicial:
```
$ rails db:seed
```
Este comando irá criar 6 imóveis com suas respectivas informações e imagens, bem como um usuário 'responsável' por esses imóveis.

## Testando a aplicação (manualmente)
Inicie um servidor em sua máquina:
```
$ rails server
```

Abra seu navegador de preferência e vá para o endereço: `'localhost:3000'`, você poderá ver a página inicial da aplicação.

Para visualizar a lista de Heróis basta criar um usuário e fazer login com o mesmo.



Ao realizar o login a lista de heróis expõe os nomes e o rank de cada um deles, com botões para realizar o CRUD básico, na página de visualização das informações de cada herói está disponível seu histórico de monstros derrotados.

Você também poderá ver na barra de navegação a opção de `Threats` onde será possível visualizar as ameaças que são criadas à partir do socket que está sendo ouvido. 

## Agradecimentos

Um grande agradecimento à equipe da ZRP pela atenção em minha candidatura à vaga, e pela oportunidade cedida em realização desse teste.
