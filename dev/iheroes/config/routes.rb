Rails.application.routes.draw do
  root to: redirect('/heros')
  devise_for :users
  resources :heros
  get 'threats', to: 'threats#index'
  post 'threats/create', to: 'threats#create'

  # Serve websocket cable requests in-process
  mount ActionCable.server => '/cable'
end
