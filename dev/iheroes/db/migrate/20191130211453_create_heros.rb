class CreateHeros < ActiveRecord::Migration[6.0]
  def change
    create_table :heros do |t|
      t.string :name
      t.string :rank
      t.decimal :lat, precision: 5, scale: 2
      t.decimal :long, precision: 5, scale: 2

      t.timestamps
    end
  end
end
