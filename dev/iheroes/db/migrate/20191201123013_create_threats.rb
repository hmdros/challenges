class CreateThreats < ActiveRecord::Migration[6.0]
  def change
    create_table :threats do |t|
      t.string :dangerlevel
      t.string :monstername
      t.decimal :lat, precision: 5, scale: 2
      t.decimal :long, precision: 5, scale: 2
      t.belongs_to :hero

      t.timestamps
    end
  end
end
