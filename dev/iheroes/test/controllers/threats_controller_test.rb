require 'test_helper'

class ThreatsControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get threats_new_url
    assert_response :success
  end

  test "should get create" do
    get threats_create_url
    assert_response :success
  end

end
