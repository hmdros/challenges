class ThreatChannel < ApplicationCable::Channel
  def subscribed
    threats = Threat.all
    stream_for threats
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end
