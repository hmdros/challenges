class HerosController < ApplicationController
  before_action :set_hero, only: %i[show edit update destroy]

  def index
    @heros = Hero.all
  end

  def show; end

  def new
    @hero = Hero.new
  end

  def create
    @hero = Hero.new(heros_params)

    respond_to do |format|
      if @hero.save
        format.html { redirect_to @hero, notice: 'Hero was successfully created.' }
        format.json { render json: @hero, status: :created, location: @hero }
      else
        format.html { render action: "new" }
        format.json { render json: @hero.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit; end

  def update
    respond_to do |format|
      if @hero.update_attributes(heros_params)
        format.html { redirect_to @hero, notice: 'Hero was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @hero.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @hero.destroy
  end

  private

  def heros_params
    params.require(:hero).permit(:name, :rank)
  end

  def set_hero
    @hero = Hero.find(params[:id])
  end
end
