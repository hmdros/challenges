class ThreatsController < ApplicationController
  def index
    @threats = Threat.last(30).reverse
  end

  def create
    @threat = Threat.new(threat_params)
    @threat.save
    respond_to do |format|
      format.js { render alert: 'NEW THREAT!' }
    end
    broadcast()
  end

  def broadcast
    threat_html = ApplicationController.renderer.render(partial: 'threats/threat', locals: { threat: @threat })
    ThreatChannel.broadcast_to @threat, threat_html: threat_html
  end

  private

  def threat_params
    params.require(:threats).permit(:dangerlevel, :monstername, :lat, :long)
  end
end
