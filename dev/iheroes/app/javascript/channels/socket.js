const socket = require('socket.io-client')('https://zrp-challenge-socket.herokuapp.com:443');
socket.on('occurrence', function(data) {
  console.log(data)
  if (data) {
    fireModal(data)
    appendLine(data)
  }

  function fireModal(data) {
    $.ajax({
      method: "POST",
      url: "/threats/create",
      data: {
        threats: {
          lat: data.location[0].lat,
          long: data.location[0].lng,
          dangerlevel: data.dangerLevel,
          monstername: data.monsterName
        }
      },
      dataType: 'json'
    }).done(swal({ text: "New Threat!!!", icon: "warning", dangerMode: true, timer: 1500 }))
  }

  function appendLine(data) {
    console.log('printing data');
    const list = document.querySelector('.threats-list');
    const html = `<div class="card col-4"><div class="card-header"><div><strong>${data.monsterName}</strong></div><div> Danger: ${data.dangerLevel}</div><img src="https://api.adorable.io/avatars/30/abott@adorable.png" alt=""></div><div class="card-body"><h6>Location</h6><p>Lat: ${data.location[0].lat} / Long: ${data.location[0].lng}</p></div><div class="card-footer"></div></div>`
    list.insertAdjacentHTML("beforebegin", html)
  }
});

export default socket;



