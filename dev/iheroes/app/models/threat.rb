class Threat < ApplicationRecord
  # validations
  validates :monstername, presence: true
  validates :dangerlevel, presence: true

  # associations
  belongs_to :hero

  # callbacks
  after_create_commit { ThreatChannel.broadcast_to(self, '<h1>New Threat</h1>') }
  before_validation :set_hero

  def set_hero
    heros = []
    case dangerlevel
    when 'God'
      heros << Hero.all.s_heros
      self.hero = heros.first.sample
    when 'Tiger'
      heros << Hero.all.s_heros
      heros << Hero.all.a_heros
      self.hero = heros.first.sample
    when 'Wolf'
      heros << Hero.all.s_heros
      heros << Hero.all.a_heros
      heros << Hero.all.b_heros
      self.hero = heros.first.sample
    when 'Wood'
      heros << Hero.all.s_heros
      heros << Hero.all.a_heros
      heros << Hero.all.b_heros
      heros << Hero.all.c_heros
      self.hero = heros.first.sample
    end
  end
end
