class Hero < ApplicationRecord
  # validations
  validates :name, presence: true
  validates :rank, presence: true

  # associations
  has_many :threats

  # scopes
  scope :a_heros, -> do
    where(rank: 'A')
  end

  scope :b_heros, -> do
    where(rank: 'B')
  end

  scope :c_heros, -> do
    where(rank: 'C')
  end

  scope :s_heros, -> do
    where(rank: 'S')
  end
end
